Ajou ndn-atmos
============

### This is pre-release software
####If you have trouble running it, send an email to dabin912@uns.ajou.ac.kr and inchan@uns.ajou.ac.kr.

 This software is designed to support ongoing climate model research at Kisti. 

 Currently, this software provides an API to publish, query and retrieve scientific datasets using
 NDN.

Dependencies
---------------------

**The Ajou ndn-atmos is built based on several libraries**

 * boost (Minimum required boost version is 1.48.0)
 * jsoncpp 1.6.0 (https://github.com/open-source-parsers/jsoncpp.git)
 * mysql 5.6.23 (http://www.mysql.com/)
 * ndn-cxx (https://github.com/named-data/ndn-cxx.git)
 * ChronoSync (https://github.com/named-data/ChronoSync.git)

**Dependency for tools and translator library**

 * python3
 * netcdf4-python3
 * mysql-connector-python3

**The ndn-cxx and ChronoSync need some other prerequisites.**

 *  For OSX, the prerequisites can be installed using Homebrew:

	brew install boost sqlite3 mysql jsoncpp hdf5 openssl cryptopp protobuf
	pip3 install mysql-connector-python --allow-all-external
	pip3 install netCDF4



 * For Ubuntu, use the command below to install the prerequisites:

	sudo apt-get install libboost-all-dev libssl-dev libcrypto++-dev \
                        libsqlite3-dev libmysqlclient-dev libjsoncpp-dev \
                        protobuf-compiler libprotobuf-dev netcdf4-python \
                        python3-mysql.connector python3-pip libhdf5-dev \
                        libnetcdf-dev python3-numpy

	sudo pip3 install netCDF4


 * For Fedora, use the command below to install the prerequisites:

	sudo yum install boost-devel openssl-devel cryptopp-devel sqlite3x-devel \
                    mysql-devel jsoncpp-devel protobuf-compiler protobuf-devel \
                    netcdf4-python3 mysql-connector-python3




Installing ndn-cxx
---------------------

* Download ndn-cxx source code. Use the link below for ndn-cxx code:

	git clone https://github.com/named-data/ndn-cxx.git

	cd ndn-cxx
  


* In library folder, build from the source code

	./waf configure --disable-static --enable-shared

	./waf

	./waf install


Installing ChronoSync
---------------------

* Download ChronoSync source code. Use the link below for the ChronoSync code:

	git clone https://github.com/named-data/ChronoSync.git

	cd ChronoSync


* Build from the source code

	./waf configure

	./waf
	
	./waf install



Installing Ajou ndn-atmos
---------------------

Follow the steps below to compile and install Ajou ndn-atmos:

* Download the ajou ndn-atmos source code. Use the command below:

	https://ajoundn@bitbucket.org/ajoundn_atmos/ajoundn-atmos.git
  
	cd ajoundn-atmos


* Build Ajou ndn-atmos in the project folder

	./waf configure

	./waf

	./waf install


* To test Ajou ndn-atmos, please use the steps below:

	./waf configure --with-tests

	./waf

	./build/catalog/unit-tests


* Note that if you are using Fedora or Ubuntu, you may need to create a configuration file for
ndn-cxx in /etc/ld.so.conf.d to include the path where the libndn-cxx.so is installed. Then
update using command below:

	ldconfig


*For example, if the libndn-cxx.so is installed in /usr/local/lib64, you need to include
this path in a "ndn-cxx.conf" file in /etc/ld.so.conf.d directory, and then run "ldconfig".


Running Ajou ndn-atmos
--------------------------

Install translator library
---------------------------
1. For the translator, ndn_cmmap_translator library is required to be in PYTHONPATH

     export PYTHONPATH="full path to /ajoundn-atmos/lib":$PYTHONPATH



Initializing Database
---------------------
* Create a database using standard mysql tool.
* You also need to create a user and set a password to connect to the database.
* Note that you will need to have actual CMIP5 data to run the tool.
* Run

	$ python3 insert_names.py

* Input full path to the filename and config file to translate
* A CMIP5 config file is located under
 /ajoundn-atmos/lib/ndn_cmmap_translators/etc/cmip5/cmip5.conf 
* Name translator will create tables named cmip5 and cmip5gloAtt and insert the names and global attributes into the tables


Starting NFD
------------
NFD is the NDN forwarding daemon.

* Download NFD source code. Use the link below for the NFD code:

	git clone https://github.com/named-data/NFD.git
	
	cd NFD

	git checkout NFD-0.3.2

	git submodule init && git submodule update
 

* Build NFD

	./waf configure

	./waf

	./waf install
 

* Run NFD

    nfd-start
 

* Note that if you are using Fedora or Ubuntu, you may need to create a configuration file for
ndn-cxx in /etc/ld.so.conf.d to include the path where the libndn-cxx.so is installed. Then
update using command below:

    ldconfig
 

*For example, if the libndn-cxx.so is installed in /usr/local/lib64, you need to include
this path in a "ndn-cxx.conf" file in /etc/ld.so.conf.d directory, and then run "ldconfig".


Launching atmos-catalog
-----------------------

* Make sure database is initialized and running

* Create catalog configuration file

    cp /usr/local/etc/ajoundn-atmos/ajoucatalog.conf.sample /usr/local/etc/ajoundn-atmos/ajoucatalog.conf
 

* Edit the configuration file /usr/local/etc/ajoundn-atmos/ajoucatalog.conf. Modify the database parameters
in both the queryAdapter and publishAdapter sections.
* Note that the database parameters in these two sections may be different to provide different
privileges.


* Run ajou ndn-atmos

    ajouatmos-catalog
 


Installing and setting up repo-ng 
---------------------------------
* Download the repo-ng and compile it. 

  + git clone https://github.com/named-data/repo-ng.git

* See install.md for more details on installation.

* Edit the default configuration file /usr/local/etc/ndn/repo-ng.conf after copy the repo-ng.conf.sample to the destination path.
  Modify its database parameter from the default value, /var/db/ndn-repo-ng to your preference.
  max-packets parameter is also to be set according to the file size to be placed in the repo.

* Data name to a particular repository is to be written under "data" section.
  + for example, we added "ndn:/ajoundn/atmos/(cmip5 file name)" above the given examples in the default configuration file.

* Repository name for a storage is to be written under "command" section.

* To run any repo-ng tools and its features, repo-ng deamon must have already been operating.
  + Use "sudo ndn-repo-ng" command to do it.

* "ndnputfile" is the tool to insert any file into a repository.

   + Example command is "$ndnputfile /example/repo/1 /ajoundn/atmos/psl_6hrPlev_MIROC5_historical_r1i1p1_1950010100-1950123118.nc psl_6hrPlev_MIROC5_historical_r1i1p1_1950010100-1950123118.nc"
 
   + 1st parameter points to the repository name for a file to be stored.
   + 2nd one is the ndn name to publish it over the network. It must be already registered in the /usr/local/etc/ndn/repo-ng.conf file prior to its execution.
   + 3rd one is the file name with its extension.

* the client side will retrieve the file from the producer.

* Beware that the name, "/ajoundn/atmos", is hard-coded in files "/usr/local/etc/ajoundn-atmos/ajoucatalog.conf", ajoundn-atmos/client/query/query.html, ajoundn-atmos/client/query/config.json, ajoundn-atmos/client/query/retrieve_agent.js
  so they must be altered when you deploy it in your system.


Installing and setting up NDN-tools 
---------------------------------------
* We need the newest NDN-tools to utilize the multiple pipeline feature
  + git clone https://github.com/dibenede/ndn-tools.git
  + cd ndn-tools
  + ./waf configure
  + ./waf
  + sudo ./waf install

* this procedure will install ndn tools to retrieve files in many-segments.



Starting front end
------------------

* Open the client folder in Ajou ndn-atmos

* Checkout the ndn-js in the client folder. Use the link blow:

	git clone http://github.com/named-data/ndn-js.git
  
	cd ndn-js
 

* Start python simple server in the client folder (ajoundn-atmos/client)

	python -m SimpleHTTPServer
 

* Open project query page in a web browser

	http://localhost:8000/query/query.html
 
