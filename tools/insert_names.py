#!/usr/bin/env python3
# -*- Mode:python; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
#
# Copyright (c) 2015, Colorado State University.
#
# This file is part of ndn-atmos.
#
# ndn-atmos is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.
#
# ndn-atmos is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
#
# You should have received copies of the GNU General Public License and GNU Lesser
# General Public License along with ndn-atmos, e.g., in COPYING.md file.  If not, see
# <http://www.gnu.org/licenses/>.
#
# See AUTHORS.md for complete list of ndn-atmos authors and contributors.

'''This module is for inserting NDN names in underlying database'''

import mysql.connector as sql
from mysql.connector import errorcode
import sys
import getpass
import hashlib
import os, shutil
import netCDF4
from ndn_cmmap_translators import atmos_translator


## We do not use ndnfs anymore. So this code block below won't be needed anymore. 
## The method below was designed to integrate NDN-Atmos with ndnfs-port. It creates hierachical directory structures and copies the CMIP5 file into it. 
"""
def pathAccumulatorAndCopy(ndnName, ndnFspath, sourcePath, timeVariable):
    srcPath = ""

    destinationToBeMade = ndnFspath+ndnName ## MountPoint and ndnName make up the destinationPath
    print ("Destination path to be under the mount point %s " %destinationToBeMade)

    ndnNameInList = ndnName.split('/')
    ndnNameInList.remove('') ##Because of '/' there are two '' empty strings in the beginning and the end of the list. We remove it ...
    ndnNameInList.remove('')

    print("NDN name is %s" %ndnNameInList)

    bond = '_' ## Bond to connect all the file name components to make a new one.

    sequence = (ndnNameInList[-1], ndnNameInList[-3], ndnNameInList[3], ndnNameInList[4], ndnNameInList[-2], timeVariable)

    fileNameNoType=bond.join(sequence)
    fileName = fileNameNoType + ".nc" ## .nc is added after all the accumulated components.
    print("My file name is %s" %fileName)


    try:
        if os.path.exists(destinationToBeMade):
            print("Directory is already there")
        else:
            os.makedirs(destinationToBeMade)
    except os.error as ose:
        print ("OS error : %s" %(ose))
    except IOError as e:
        print ("I/O error({0}) ".format(e.strerror))
    except:
        print("Unexpected Error:", sys.exc_info()[0])

    try:
        if os.path.isdir(sourcePath):
            srcPath = sourcePath + '/' + fileName
            print("the path is directory!")
        else:
            srcPath = sourcePath
            print("There is a file in the path.")
    except os.error as ose:
        print ("OS error : %s" %(ose))


    try:
        if not os.path.isfile(destinationToBeMade+fileName): ## We check whether there is already a file if there is any file already stored in the destination directory.
                                                           ## We escape to be next next
            shutil.copy2(srcPath, destinationToBeMade)
        else:
            print("File with the same name is already there!")
            return ##escape function

    except shutil.Error as shuEr:
        print ('Error %s' %shuEr)
    except IOError as IOe:
        print ('Error %s' %IOe)

"""




def createTables(cursor):
  TABLES = {}
  TABLES['cmip5'] = (
    "CREATE TABLE `cmip5` ("
    "  `id` int(100) AUTO_INCREMENT NOT NULL,"
    "  `sha256` varchar(64) UNIQUE NOT NULL,"
    "  `name` varchar(1000) NOT NULL,"
    "  `activity` varchar(100) NOT NULL,"
    "  `product` varchar(100) NOT NULL,"
    "  `organization` varchar(100) NOT NULL,"
    "  `model` varchar(100) NOT NULL,"
    "  `experiment` varchar(100) NOT NULL,"
    "  `frequency` varchar(100) NOT NULL,"
    "  `modeling_realm` varchar(100) NOT NULL,"
    "  `mip_table` varchar(100) NOT NULL,"
    "  `ensemble` varchar(100) NOT NULL,"
    "  `variable_name` varchar(100) NOT NULL,"
    "  `time` varchar(100) NOT NULL,"
    "  PRIMARY KEY (`id`)"
    ") ENGINE=InnoDB")

  #check if tables exist, if not create them
  #create tables per schema in the wiki
  for tableName, query in TABLES.items():
    try:
      print("Creating table {}: ".format(tableName))
      cursor.execute(query)
      print("Created table {}: ".format(tableName))
      return True
    except sql.Error as err:
      if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
        print("Table already exists: %s " %(tableName))
        return True
      else:
        print("Failed to create table: %s" %(err.msg))
        return False


def dbInsertGlobalAttributeCreateTable(cursor):
    ##Table name parameter is required later
    TABLES = {}
    TABLES['cmip5gloAtt'] = (
        "CREATE TABLE `cmip5gloAtt` ("
        " `id` int(100) AUTO_INCREMENT NOT NULL,"
        " `institution` varchar(100) NOT NULL,"
        " `source` varchar(100) NOT NULL,"
        " `forcing` varchar(100) NOT NULL,"
        " `parent_experiment_id` varchar(100) NOT NULL,"
        " `branch_time` varchar(100) NOT NULL,"
        " `contact` varchar(100) NOT NULL,"
        " `initialization_method` varchar(100) NOT NULL,"
        " `physics_version` varchar(100) NOT NULL,"
        " `tracking_id` varchar(100) NOT NULL,"
        " `experiment` varchar(100) NOT NULL,"
        " `creation_date` varchar(100) NOT NULL,"
        " `Conventions` varchar(100) NOT NULL,"
        " `table_id` varchar(100) NOT NULL,"
        " `parent_experiment` varchar(100) NOT NULL,"
        " `realization` varchar(100) NOT NULL,"
        " `cmor_version` varchar(100) NOT NULL,"
        " `comments` varchar(100) NOT NULL,"
        " `history` varchar(100) NOT NULL,"
        " `cmipreferences` varchar(100) NOT NULL,"
        " `title` varchar(100) NOT NULL,"
        "  PRIMARY KEY (`id`)"
    ") ENGINE=InnoDB")


    for tableName, query in TABLES.items():
        try:
            cursor.execute(query)
            return True
        except sql.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print ("Table is already there %s" %(tableName))
                return True
            else:
                print("Error is here %s" %(err.msg))
                return False


def insertAttributeValues(cursor, values):
    tupleValues = tuple(values)

    addRecord = ("INSERT INTO cmip5gloAtt"
                 "(institution, source, forcing, parent_experiment_id, branch_time, contact, initialization_method, physics_version, tracking_id, experiment, creation_date, Conventions, table_id, parent_experiment, realization, cmor_version, comments, history, cmipreferences, title) "
                 "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")



    print(tupleValues)

    try:
        cursor.execute(addRecord, tupleValues)
    except sql.error as err:
        print("Error inserting name %s" %(err.msg))






def insertName(cursor, name, time):
  if __debug__:
    print("Name to insert %s " %(name))

  #hashvalue is needed since name is too long for primary key (must be <767 bytes)
  hashValue =  hashlib.sha256(str(name).encode('utf-8')).hexdigest()
  if __debug__:
    print("Hash of name %s " %(hashValue))

  ##NOTE:must use %s to prevent sql injection in all sql queries
  splitName = list(filter(None, name.split("/")))
  splitName.insert(0, hashValue)
  splitName.insert(0, name)
  splitName.append(time)
  splitName = tuple(splitName)

  if __debug__:
    print("Name to insert in database %s" %(splitName,))

  addRecord = ("INSERT INTO cmip5 "
               "(name, sha256, activity, product, organization, model, experiment, frequency,\
               modeling_realm, mip_table, ensemble, variable_name, time) "
               "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")

  try:
    cursor.execute(addRecord, splitName)
    print("Inserted record %s" %(name))
    return True
  except sql.Error as err:
    print("Error inserting name %s, \nError:%s" %(name, err.msg))
    return False

if __name__ == '__main__':
  globalAttributes = ['institution','source', 'forcing', 'parent_experiment_id','branch_time', 'contact', 'initialization_method', 'physics_version', 'tracking_id', 'experiment', 'creation_date', 'Conventions', 'table_id', 'parent_experiment', 'realization', 'cmor_version', 'comments', 'history', 'references', 'title']
  valueList = list()
  ##globalAttributes = ['institution','source', 'forcing', 'parent_experiment_id','branch_time', 'contact', 'initialization_method', 'physics_version', 'tracking_id', 'experiment', 'creation_date', 'Conventions', 'table_id', 'parent_experiment', 'realization', 'cmor_version', 'comments', 'history', 'references', 'title']
  datafilePath = input("File/directory to translate: ")
  configFilepath = input("Schema file path for the above file/directory: ")
  ##ndnfspath = input("NDNfs mount point path: ") No more ndnfs
  ##this code gets an user input for NDNFS-port mount point

  #do the translation, the library should provide error messages, if any
  ndnNames = atmos_translator.argsForTranslation(datafilePath, configFilepath)
  times = atmos_translator.timeGetter(datafilePath, configFilepath) ##Time must be read from files not cmip5.conf
  valueList = atmos_translator.gloAttriGetter(datafilePath, configFilepath)
  ##Initially, it was a mipName getter but now it reads times. MipNames are retrieved by cmip5.conf file.



  if ndnNames is False:
    print("Error parsing config file, exiting")
    sys.exit(-1)

  if len(ndnNames) == 0:
    print("No name returned from the translator, exiting.")
    sys.exit(-1)
  if __debug__:
    print("Returned NDN names: %s" %(ndnNames))
   ## print("Returned MipNames: %s" %(times))

  #open connection to db
  dbHost = input("Database Host?")
  dbName = input("Database to store NDN names?")
  dbUsername = input("Database username?")
  dbPasswd = getpass.getpass("Database password?")




  try:
    con = sql.connect(user=dbUsername, database=dbName, password=dbPasswd, host=dbHost)
    cursor = con.cursor()
    if __debug__:
      print("successfully connected to database")



    #if tables do not exist, create tables
    #if table already exists, or creation successful, continue



    if createTables(cursor):
      for ndnName, time in list(zip(ndnNames,times)):
        #get list of files and insert into database
        resInsertnames = insertName(cursor, ndnName,time)

        ## This code block was designed for the ICTC ndnfs aided Ajou NDN Atmos. We switched it to repo-ng now. We don't need it anymore.
        ## the block below makes the fuse integration method work. 
        """if resInsertnames:
            pathAccumulatorAndCopy(ndnName,ndnfspath,datafilePath,time)
            print("File copied!")
        else:
            print("file copy did not take place.")""" 


        if __debug__:
          print("Return Code is %s for inserting name: %s, Mip_Table : %s" %(resInsertnames, ndnName, time))
      con.commit()

      ##After the creation of cmip5 table, globalAttribute table is made.
      if dbInsertGlobalAttributeCreateTable(cursor):
          for aGlobalAtt in valueList:
              boolResult = insertAttributeValues(cursor, aGlobalAtt)

              print("Return Code %s" %(boolResult))

          foreignkey = "ALTER TABLE `cmip5` ADD FOREIGN KEY (`id`) REFERENCES `cmip5gloAtt` (`id`) "
          try:
             cursor.execute(foreignkey)
          except sql.error as err:
             print ("Error foreign key assignment %s" %(err.msg))

          con.commit()

      else:
        print("Creation table failed")
        sys.exit(-1)



    else:
      print("Error creating tables, exiting.")
      sys.exit(-1)



  except sql.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
      print("Incorrect username or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
      print("Database does not exist")
    else:
      print("Error connecting to Database: %s" %(err.msg))
  finally:
    con.close()

  print("Content logging : %s" %(ndnNames))
  print("Datafile path : %s" %(datafilePath))
