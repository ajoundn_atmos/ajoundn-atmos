
//vartika 20151005 - begins
var globalConfig;


(function(){
  "use strict";
  var config;
  Promise.all([
    new Promise(function(resolve, reject){
      $.ajax('config.json').done(function(data){
        config = data;
        resolve();
      }).fail(function(){
        console.error("Failed to get config.");
        //ga('send', 'event', 'error', 'config');
        reject();
      });
    }),
    new Promise(function(resolve, reject){
      var timeout = setTimeout(function(){
        console.error("Document never loaded? Something bad has happened!");
        reject();
      }, 10000);
      $(function () {
        clearTimeout(timeout);
        resolve();
      });
    })
  ]).then(function(){
    
	globalConfig = config;
  //setupRequestForm();
      console.log("Ajou");
  }, function(){
    console.error("Failed to initialize!");
    //ga('send', 'event', 'error', 'init');
  });
})();
//vartika 20151005 - ends


var detailHeader0 = "Metadata_Name"
var detailHeader1="Metadata_Value";

var testname;
function initializeDetailsTable() {
	console.log("vartika in initializeDetailsTable ");

	var detailsTableHTML = "<table align=\"center\" class=\"detailTable tablesorter dialog-data-table\"></table>";
	
	$(".dialog-data").append(detailsTableHTML);
	

	var detailsTable = $(".dialog-data-table"); 
 
	var headers = "<th class=tablesorter-header>" + detailHeader0 + "</th>" +
		   "<th class=tablesorter-header>" + detailHeader1 + "</th>" ;
		 	   		
	detailsTable.append("<thead><tr>" + headers + "</tr></thead>");
}


function buildRowsInDetailsTable(i) {
	var detailsTable = $(".dialog-data-table");
	var col1 = "", col2 = "";
	var metadata = results[i];
	detailsTable.append("<tbody>");
    
    for (var key in metadata) {
        if (metadata.hasOwnProperty(key)) { 
            console.log("vartika says " + key + " -> " + metadata[key]); 
            col1 = "<td> " + key + "</td>"; 
            col2 = "<td> " +metadata[key] + "</td>";  
            
            var rowElement = col1 + col2;  
            detailsTable.append('<tr>' + rowElement +'</tr>'); 
        }
	}    	        
	detailsTable.append("</tbody>");
	
}

function createDetailsTable(i)	{
	
	console.log("vartika in createDetailsTable ");
	initializeDetailsTable();
	buildRowsInDetailsTable(i);
	var detailsTable = $(".dialog-data-table");
        detailsTable.show();
	
	detailsTable.tablesorter( {sortList: [[0,0] ]} );
}


//from the plugin available at 
//http://www.the-art-of-web.com/search-highlight-demo.html?PageSpeed=off
var myHilitor; 
 
document.addEventListener("DOMContentLoaded", function() {
    myHilitor = new Hilitor("details");
    myHilitor.setMatchType("left");
}, false);

var searchTrigger = function() {
    var searchEl = document.getElementById("searchBoxId");
    myHilitor.apply(searchEl.value);
}

function myFunc1() {
}
/*
function createHilitorBox()	{
    
    cmoreName = "log";
    var downloadPath = "ndn:/catalog/myUniqueName/" + cmoreName;
	var detailsTable = $(".dialog-data-table");
	var form = "<form action=\"\" method=\"POST\">";
    
    // 2015.07.12 added by dabin for download ----------
    var downloadButton = "<INPUT TYPE=\"button\" VALUE=\"Download\" onClick=\"location.href=\'" + downloadPath + ".pdf\'\"><br>";
    //--------------------end dabin --------------------
	var searchBox = "<input type=\"search\" onKeyUp=\"searchTrigger();\" onclick=\"searchTrigger()\" id=\"searchBoxId\" class=\"searchBox\" placeholder=\"Search for something..\">"; 
	var searchButton =  ""; //"<input type=\"button\" name=\"go\" value=\"searchTrigger()\"> ";   
	var formTagClose = "</form>";

	//var searchElement = form+searchBox+searchButton+formTagClose;
    // 2015.07.12 modified by dabin
    var searchElement = form+downloadButton+searchBox+searchButton+formTagClose;

	$(searchElement).insertBefore(detailsTable);  
                 
}*/
function createHilitorBox()	{
	var searchBox = "<input type=\"search\" onKeyUp=\"searchTrigger();\" onclick=\"searchTrigger()\" id=\"searchBoxId\" class=\"searchBox\" placeholder=\"Hilight text...\">"; 
	var detailsTable = $(".dialog-data-table");
    	var searchElement = searchBox;
	$(searchElement).insertBefore(detailsTable);           
}


//-------------------------------2015.10.08 added by dabin for repo-ng retrieval----------

//var pipelineSize = 4;
//var retransmitssionDict = {};

function retrieveData(cmorName) {
    console.log ("retrieveData() is called");
            
    //---------------------------- key ----------------------------------------------
    //Key setup
    var keyAdded = false;
    
    if (!keyAdded){
        if (!globalConfig.retrieval.demoKey || !globalConfig.retrieval.demoKey.pub ||
            !globalConfig.retrieval.demoKey.priv){
            createAlert("This host was not configured to handle retrieval! See console for details.", 'alert-danger');
            console.error("Missing/invalid key! This must be configured in the config on the server.", config.demoKey);
            return;
        }
        
        //FIXME base64 may or may not exist in other browsers. Need a new polyfill.
        var pub = new Buffer(base64.toByteArray(globalConfig.retrieval.demoKey.pub)); //MUST be a Buffer (Buffer != Uint8Array)
        var priv = new Buffer(base64.toByteArray(globalConfig.retrieval.demoKey.priv));
        var identityStorage = new MemoryIdentityStorage();
        var privateKeyStorage = new MemoryPrivateKeyStorage();
        
        keyChain = new KeyChain(new IdentityManager(identityStorage, privateKeyStorage),
                                new SelfVerifyPolicyManager(identityStorage));

        var keyName = new Name("/retrieve/DSK-123");
        certificateName = keyName.getSubName(0, keyName.size() - 1)
            .append("KEY").append(keyName.get(-1))
            .append("ID-CERT").append("0");
        
        identityStorage.addKey(keyName, KeyType.RSA, new Blob(pub, false));
        privateKeyStorage.setKeyPairForKeyName(keyName, KeyType.RSA, pub, priv);
        
        face.setCommandSigningInfo(keyChain, certificateName);
        keyAdded = true;
    }  
    
    //-----------------------------------end --------------------------------------------
    
    var retrievePrefix = new Name ("/catalog/ui" + guid());
    
    var sendTimer = setTimeout(function(){
        var prefix = new Name ("/retrieve/ucar");    
        prefix.append (retrievePrefix);
        
        // append cmor name
        prefix.append(cmorName);
        
        console.log ("whole prefix: " + prefix.toUri());
        var interest = new Interest (prefix);
        interest.setInterestLifetimeMilliseconds (3000);
                
        face.expressInterest(interest,
                                   function (interest, data){ // success
            console.log ("Request for ", prefix.toUri(), "successded", interest, data);
        },
                               function (interest){ // Failure
            console.error ("Request for", prefix.toUri(), "timed out.", interest);
        }
                                  );
    },1*1000); // wait 5 second
    
}


var guid = function(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
  }


//----------------------------------end dabin -------------------------------------

function createDownloadButton(cmorName) 	{
	//cmoreName = "log";
        console.log("File Download will be" + cmorName);
    	var downloadPath = "ndn:/catalog/myUniqueName/" + cmorName;
	var detailsTable = $(".dialog-data-table");
	var form = "<form action=\"\" method=\"POST\">";
    	//var downloadButton = "<INPUT TYPE=\"button\" VALUE=\"Download\" onClick=\"location.href=\'" + downloadPath + "\'\"><br>";
        var downloadButton = "<INPUT TYPE=\"button\" VALUE=\"Download\" onClick=retrieveData(\'"+cmorName+"\')>";
	var formTagClose = "</form>";
    	var downloadElement = form+downloadButton+formTagClose;
		$(downloadElement).insertBefore(detailsTable);  
}

function initializeDialog()	{
    
    var dialogElement = $(".dialog-modal");
    //dialogElement.dialog({ height:400, modal: true, color:'red'});
    dialogElement.dialog({modal: true,  width:'auto'});  
    //vartika did some temporary bug fix here - begins
    /* *here is issue with positioning dialog 
      ** need to use jQuery plugin called jQuery center by Andreas Lagerkvist
	http://cnedelcu.blogspot.kr/2014/02/jquery-ui-dialog-centering-ultimate-fix.html */
    dialogElement.dialog("widget").css({ left: 100, top: 100});
    //dialogElement.dialog("widget").css({ left: 100, top: 100, bottom: -100, right: 100 });
    // $dialog.dialog("widget").css("max-width", $(window).width() - 100);
    // $dialog.dialog("option", "maxHeight", $(window).height() - 100);
    //vartika did some temporary bug fix here - ends

    dialogElement.show();
}

function displayDetails(cell, cmorName) {
    //console.log("displayDetails() is called "+ cell.text());
    console.log("displayDetails() is called "+ cell);
    

   initializeDialog();

    var contentElement = $(".dialog-data");
    contentElement.empty();

    var position = cell; //cell.attr('pos');

    var cmip5Name = (results[position])['name'];
    console.log(cmip5Name);
    contentElement.append("<b>MetaData for " + cmip5Name + "<b><br/><br/>");

    createDetailsTable(position);
    createHilitorBox();
    createDownloadButton(cmorName);
    
    testname = cmorName;
}


