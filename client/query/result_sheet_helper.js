var header0 = "";
var header1 = "name";
var header2 = "activity";
var header3 = "product";
var header4 = "organization";
var header5 = "model";
var header6 = "experiment";
var header7 = "frequency";
var header8 = "modeling_realm";
var header9 = "variable_name";
var header10 = "ensemble";
var header11 = "time";
var header12 = "mip_table";


function initializeResultsTable() {
    resultTable.empty();
	
	var headers = "<th class=tablesorter-header transbox>" + header0 + "</th>" +
                   "<th class=tablesorter-header transbox>" + header1 + "</th>" +
		   "<th class=tablesorter-header transbox>" + header2 + "</th>" +
		   "<th class=tablesorter-header transbox>" + header3 + "</th>" +
		   "<th class=tablesorter-header transbox>" + header4 + "</th>" +
   		   "<th class=tablesorter-header transbox>" + header5 + "</th>" +
		   "<th class=tablesorter-header transbox>" + header6 + "</th>" +
           	   "<th class=tablesorter-header transbox>" + header7 + "</th>" +
    	   	   "<th class=tablesorter-header transbox>" + header8 + "</th>" +
           	   "<th class=tablesorter-header transbox>" + header9 + "</th>" +
           	   "<th class=tablesorter-header transbox>" + header10 + "</th>" +
  	   	   "<th class=tablesorter-header transbox>" + header11 + "</th> "+
                   "<th class=tablesorter-header transbox>" + header12 + "</th> ";	   		
    
	resultTable.append("<thead><tr>" + headers + "</tr></thead><tbody>");
}

function resultClickHandler(i, cmorName) {
        console.log("Result click handler method is called now.");	
//var cell = $(this).find('td:first');
	var cell = $(self).find('td:first');
	//var cell = resultTable.find('td:first');
	var text = cell.text();	
	//alert("vartika" + text);
    console.log("Before displayDetails() is called");
	displayDetails(i, cmorName);
           
}

function buildRow(i) {
    console.log("buildRow () is called");

    var resVartika = Object.keys(results[i])[0];//vartika Object.keys(results[0])[0]
    console.log("resVartika: " + resVartika);
    
    var rowId = ' id=\"row'+ i + "\"";
    // 2015.07.08 commented by dabin
    console.log("rowId: "+rowId);
	
    var pos = ' pos=' +i;
    // 2015.07.08 commented by dabin
    console.log("pos: "+pos);

    var col0 = "", col1 = "", col2 = "", col3 = "", col4 = "", col5 = "", col6 = "", col7 = "", col8 = "", col9 = "", col10 = "";
	var metadata = results[i];
    
    // 2015.07.08 commented by dabin
    console.log("result["+i+"]: "+results[i]);
    //////////////// end dabin////////////////////////////////////////////
    
    var n_name;
    var n_activity;
    var n_product;
    var n_org;
    var n_model;
    var n_experim;
    var n_freq;
    var n_realm;
    var n_variable;
    var n_ensem;
    var n_time;
    var n_mip_table;
    
    
    for(var key in metadata){
        if(metadata.hasOwnProperty(key)){
            console.log("vartika says " + key + " -> " + metadata[key]);
            if(key == header1){
                col1 = "<td "  + rowId + pos + " > " + metadata[key] + "</td>";   // name
                n_name = metadata[key];
            }
            if(key == header2){
                col2 = "<td> " + metadata[key] + "</td>";   //activity
                n_activity = metadata[key];
            }
            if(key == header3){
                col3 = "<td> " + metadata[key] + "</td>";   //product
                n_product = metadata[key];
            }
            if(key == header4){
                col4 = "<td> " + metadata[key] + "</td>";   //organization
                n_org = metadata[key];
            }
            if(key == header5){
                col5 = "<td> " + metadata[key] + "</td>";   //model
                n_model = metadata[key];
            }
            if(key == header6){
                col6 = "<td> " + metadata[key] + "</td>";   //experiment
                n_experim =  metadata[key];
            }
            if(key == header7){
                col7 = "<td> " + metadata[key] + "</td>";   //frequency
                n_freq = metadata[key];
            }
            if(key == header8) {
                col8 = "<td> " + metadata[key] + "</td>";   //modeling_realm
                n_realm = metadata[key];
            }
            if(key == header9) {
                col9 = "<td> " + metadata[key] + "</td>";   // variable_name
                n_variable = metadata[key];
            }
            if(key == header10) {
                col10 = "<td> " + metadata[key] + "</td>";  //ensemble
                n_ensem = metadata[key];
            }
            if(key == header11) {
                col11 = "<td> " + metadata[key] + "</td>";  //time
                n_time = metadata[key];
            }
            if(key == header12) {
                col12 = "<td> " + metadata[key] + "</td>"; //mip_table
                n_mip_table = metadata[key];
            }
        }
    }
	var rowElement = '<td' + rowId + pos + '>' + i+ '</td>';
    
	//rowElement += col0+ col1 + col2 + col3 + col4 + col5 + col6 + col7 + col8 + col9 + col10;
     var esgfDataName = "/"+ n_activity +"/"+ n_product +"/"+n_org + "/" + n_model + "/" + n_experim + "/" + n_freq + "/" + n_realm +"/";
         esgfDataName += n_mip_table + "/" + n_ensem + "/" + n_variable + "/";
    
     var cmoreNameTemp = '\''+n_variable +"_"+ n_mip_table + "_" + n_model + "_" + n_experim + "_" + n_ensem + "_" + n_time +".nc"+'\'';
 //var cmoreNameTemp = n_variable +"MIPtable" + n_experim  + n_ensem + n_time +"nc";
var cmoreName = encodeURI(cmoreNameTemp);
     esgfDataName += cmoreName;
    
    //console.log("ESGF name: " + esgfDataName);
    col1 = "<td> " + esgfDataName + "</td>";
    
    rowElement += col1 + col2 + col3 + col4 + col5 + col6 + col7 + col8 + col9 + col10 + col11 + col12;   
    
    //resultTable.append('<tr onclick=resultClickHandler(' + i + ');>' + rowElement +'</tr>');//vartika 

var params = encodeURI( i + "," + cmoreName); 

    resultTable.append("<tr onclick=resultClickHandler(" +  params + ");>" + rowElement +"</tr>");//vartika     
    var row = resultTable.find("tr")[i]; 
    console.log("File name nc is here , " + cmoreName );

    
    console.log ("Register onclick method to  tr (" + i +")");
    
  
}

